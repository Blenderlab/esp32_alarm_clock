#include <MD_REncoder.h>


#define ROTARY_ENCODER_A_PIN 5
#define ROTARY_ENCODER_B_PIN 18
#define ROTARY_ENCODER_BUTTON_PIN 19 

int buttonState;             // the current reading from the input pin
int lastButtonState = HIGH;   // the previous reading from the input pinP

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 40;    // the debounce time; increase if the output flickers

MD_REncoder R = MD_REncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN);


void rotary_onButtonClick() {
  if ( alarm_ring) {
    alarm_ring = false;
    return;
  }
  changed = true;
  Serial.println("Click!");
  manage_menu();
}



void rotary_loop() {
  int reading = digitalRead(ROTARY_ENCODER_BUTTON_PIN);
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != buttonState) {
      buttonState = reading;
      if (buttonState == HIGH) {
        Serial.println('Click');
        rotary_onButtonClick();
      }
    }
  }

  lastButtonState = reading;
  uint8_t x = R.read();
  if (x)
  {
    changed = true;

    if (menu_state == MENU_ALARM_SET) {
      if (submenu == 0) {
        if (x == DIR_CW ) {
          alarmh++;
          if (alarmh >= 24) alarmh = 0;
        }  else {
          alarmh--;
          if (alarmh < 0) alarmh = 23;
        }
      }

      if (submenu == 1) {

        if (x == DIR_CW ) {
          alarmm++;
          if (alarmm >= 59) alarmm = 0;

        }  else {
          alarmm--;
          if (alarmm < 0) alarmm = 59;

        }

      }

    } else {

      if (x == DIR_CW ) {
        menu_pos--;
        if (menu_pos <= 0) {
          menu_pos = 0;
        }
      }  else {
        menu_pos++;
        if (menu_pos >= 3) {
          menu_pos = 3;
        }
      }
    }
  }
}
void setup_encoder() {
  pinMode(ROTARY_ENCODER_BUTTON_PIN, INPUT_PULLDOWN);
  digitalWrite(ROTARY_ENCODER_BUTTON_PIN, LOW);
  R.begin();
}
