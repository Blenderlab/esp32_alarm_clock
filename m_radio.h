#include "Audio.h" //see my repository at github "https://github.com/schreibfaul1/ESP32-audioI2S"

#define I2S_DOUT      25
#define I2S_BCLK      27
#define I2S_LRC       26

Audio audio;

String stations[] = {
  "http://icecast.radiofrance.fr/fip-hifi.aac",
  "http://178.33.237.20:8000/stream.mp3",
  "http://37.59.104.28:8030/planetefm.mp3"
};

void setup_radio() {
  audio.setPinout(I2S_BCLK, I2S_LRC, I2S_DOUT);
  audio.setVolume(50); // 0...21
  audio.connecttohost(stations[alarmc]);
}

void update_radio() {

  if (alarm_ring || radio_state==1){
    audio.loop();
  }
  
}
