#include "Arduino.h"
#include <WiFi.h>
#include "EEPROM.h"
#define EEPROM_SIZE 64
// 0 : alarm hour  (alarmh)
// 1 : alarm minutes (alarmm)
// 2 : alarm set (alarm_set)
// 3 : radio channel (alarmc)

const char* ssid       = "LaCabane";
const char* password   = "123NousIronsAuBois";

short  menu_state ;
short menu_pos;
short alarmh = 20;
short alarmm = 17;
short alarmc = 0;
short radio_state=0; // Is radio playing or not ?
bool alarm_set = false;

short submenu = 0;
bool alarm_ring = false;
bool alarm_invert = false;

#include "m_radio.h"
#include "m_clock.h"
#include "m_menu.h"
//#include "m_oled.h"
#include "m_hd1604.h"
#include "m_encoder.h"


void setup(void) {
  //setup_oled();
  setup_lcd();
  Serial.begin(115200);
  display_status("Wifi : ... ");
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Wifi CONNECTED");
  display_status("Wifi Connected");
  delay(1000);

  display_status("Clock : ...");
  delay(1000);
  setup_clock();
  update_time();
  display_status("Clock : OK");
  delay(1000);

  if (!EEPROM.begin(EEPROM_SIZE))
  {
    Serial.println("failed to initialise EEPROM"); delay(1000000);
  }
  alarmh =   EEPROM.read(0);
  alarmm =   EEPROM.read(1);
  Serial.println(alarmh);
  Serial.println(alarmm);
  if (alarmh > 24)  EEPROM.write(0, 20);
  if (alarmm > 59)  EEPROM.write(1, 30);

  alarmh =   EEPROM.read(0);
  alarmm =   EEPROM.read(1);
  alarm_set =   EEPROM.read(2);
  alarmc =   EEPROM.read(3);
  if (alarmc > 3)    {
    EEPROM.write(3, 0);
    alarmc = 0;
  }

  EEPROM.commit();
  setup_encoder();
  menu_state = MENU_NONE;
  changed=false;
  setup_radio();
}

void loop(void) {
  rotary_loop();
  //update_oled();// Carefull update_oled is time consuming....
  update_lcd();
  update_time();
  update_radio();
  events();
}
