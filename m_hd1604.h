#include <LiquidCrystal_I2C.h>

#define Row0 0
#define Row1 1
#define Row2 2
#define Row3 3
String upperSec = "0";
String underSec = "0";


long last_update = 0;

LiquidCrystal_I2C lcd(0x27, 20, 4);
// NodeMCU Dev Kit => D1 = SCL, D2 = SDA

char *Mon[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
char *Day[] = {"DIM", "LUN", "MAR", "MER", "JEU", "VEN", "SAM"};


#define A_ 10
#define B_ 11
#define C_ 12
#define D_ 13
#define E_ 14
#define F_ 15
#define G_ 16
#define H_ 17
#define I_ 18
#define J_ 19
#define K_ 20
#define L_ 21
#define M_ 22
#define N_ 23
#define O_ 24
#define P_ 25
#define Q_ 26
#define R_ 27
#define S_ 28
#define T_ 29
#define U_ 30
#define V_ 31
#define W_ 32
#define X_ 33
#define Y_ 34
#define Z_ 35
#define B 0x20
unsigned long currentMillis = 0;
unsigned long previousMillis = 0;
unsigned long scrollMillis = 0;
unsigned long lastUpdateTime = 0;
bool dots = false;

void DefineLargeChar() {

  uint8_t cc1[8] = { // Custom Character 1
    B11100,
    B11110,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111
  };

  uint8_t cc2[8] = { // Custom Character 2
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000,
    B00000
  };

  uint8_t cc3[8] = { // Custom Character 3
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111
  };

  uint8_t cc4[8] = { // Custom Character 4
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B01111,
    B00111
  };

  uint8_t cc5[8] = { // Custom Character 5
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000
  };

  uint8_t cc6[8] = { // Custom Character 6
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000
  };

  uint8_t cc7[8] = { // Custom Character 7
    B00000,
    B11100,
    B11110,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111
  };

  uint8_t cc8[8] = { // Custom Character 8
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111
  };

  // send custom characters to the display
  lcd.createChar(1, cc1);
  lcd.createChar(2, cc2);
  lcd.createChar(3, cc3);
  lcd.createChar(4, cc4);
  lcd.createChar(5, cc5);
  lcd.createChar(6, cc6);
  lcd.createChar(7, cc7);
  lcd.createChar(8, cc8);
}



// 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z BLANK
char bn1[] = {
  8, 2, 1, 2, 1, B, 2, 2, 1, 2, 2, 1, 3, B, 8, 8, 2, 2, 8, 2, 2, 2, 2, 8, 8, 2, 1, 8, 2, 1, 8, 2, 1, 8, 2, 1, 3, 2, 2, 8, 2, 1, 8, 2, 2, 8, 2, 2, 8, 2, 1, 8, B, 8, B, 8, B, B, B, 8, 8, B, 8, 8, B, B, 8, 3, 1, 8, B, 1, 8, 2, 1, 8, 2, 1, 8, 2, 1, 8, 2, 1, 8, 2, 5, 2, 8, 2, 8, B, 8, 8, B, 8, 8, B, 8, 8, B, 8, 8, B, 8, 2, 2, 1, B, B, B
};
char bn2[] = {
  8, B, 8, B, 8, B , 3, 2, 2, B, 6, 1, 4, 3, 8, 5, 6, 7, 8, 6, 7, B, 3, 2, 8, 6, 8, 5, 6, 8, 8, 6, 8, 8, 6, 1, 8, B, B, 8, B, 8, 8, 6, B, 8, 6, B, 8, B, 3, 8, 6, 8, B, 8, B, B, B, 8, 8, 6, 2, 8, B, B, 8, B, 8, 8, 4, 8, 8, B, 8, 8, 2, 2, 8, B, 8, 8, 3, 8, 2, 2, 1, B, 8, B, 8, B, 8, 8, B, 8, 8, B, 8, B, 6, B, 2, 3, 2, B, 6, B, B, B, B
};
char bn3[] = {
  4, 3, 8, 3, 8, 3, 8, 3, 3, 3, 3, 8, B, B, 8, 4, 3, 8, 4, 3, 8, B, 8, B, 4, 3, 8, B, B, 8, 8, B, 8, 8, 3, 8, 4, 3, 3, 8, 3, 8, 8, 3, 3, 8, B, B, 4, 3, 8, 8, B, 8, B, 8, B, 4, 8, B, 8, B, 1, 8, 3, 3, 8, B, 8, 8, B, 8, 4, 3, 8, 8, B, B, 4, 3, 7, 8, B, 1, 4, 3, 8, B, 8, B, 4, 3, 8, 2, 3, 2, 4, 2, 8, 8, B, 8, B, 8, B, 4, 3, 3, B, B, B
};


void printTwoNumber(uint8_t number, uint8_t position)//13
{
  // Print position is hardcoded
  int digit0; // To represent the ones
  int digit1; // To represent the tens
  digit0 = number % 10;
  digit1 = number / 10;

  // Line 1 of the two-digit number
  lcd.setCursor(position, 0);
  lcd.write(bn1[digit1 * 3]);
  lcd.write(bn1[digit1 * 3 + 1]);
  lcd.write(bn1[digit1 * 3 + 2]);
  lcd.write(B); // Blank
  lcd.write(bn1[digit0 * 3]);
  lcd.write(bn1[digit0 * 3 + 1]);
  lcd.write(bn1[digit0 * 3 + 2]);

  // Line 2 of the two-digit number
  lcd.setCursor(position, 1);
  lcd.write(bn2[digit1 * 3]);
  lcd.write(bn2[digit1 * 3 + 1]);
  lcd.write(bn2[digit1 * 3 + 2]);
  lcd.write(B); // Blank
  lcd.write(bn2[digit0 * 3]);
  lcd.write(bn2[digit0 * 3 + 1]);
  lcd.write(bn2[digit0 * 3 + 2]);

  // Line 3 of the two-digit number
  lcd.setCursor(position, 2);
  lcd.write(bn3[digit1 * 3]);
  lcd.write(bn3[digit1 * 3 + 1]);
  lcd.write(bn3[digit1 * 3 + 2]);
  lcd.write(B); // Blank
  lcd.write(bn3[digit0 * 3]);
  lcd.write(bn3[digit0 * 3 + 1]);
  lcd.write(bn3[digit0 * 3 + 2]);
}

void printColons(uint8_t position)
{
  lcd.setCursor(position, 0);
  lcd.write (3);
  lcd.setCursor(position, 2);
  lcd.write (6);
}

void printNoColons(uint8_t position)
{
  lcd.setCursor(position, 0);
  lcd.write (B);
  lcd.setCursor(position, 2);
  lcd.write (B);
}

void setup_lcd() {

  lcd.init();
  lcd.backlight();
  DefineLargeChar(); // Create the custom characters
  lcd.clear();

}


void display_menu(int _menu) {
  lcd.clear();
  lcd.setCursor(0, 0);

  for (int i = 0; i <= 3; i++) {
    lcd.setCursor(2, i);
    lcd.print(menu[_menu][i]);
  }
  lcd.setCursor(0,  menu_pos);
  lcd.print(">");
  if (menu_state==MENU_NONE) lcd.clear();
}


void display_time( ) {
  if (dots) printColons(9); else printNoColons(9);
  printTwoNumber(hour() + 2, 1);
  printTwoNumber(minute(), 11);
  lcd.setCursor(19, 0); lcd.print(second() / 10);
  lcd.setCursor(19, 1); lcd.print(second() % 10);
  if (alarm_set) {
    lcd.setCursor(15, 3);

    if (alarmh<10) lcd.print("0");
    lcd.print(String(alarmh)+":");
    if (alarmm<10) lcd.print("0");
    lcd.print(String(alarmm));
  
  }
  lcd.setCursor(0, 3); lcd.print(String(day()) + " "+String(Mon[month()-1]));
  
}

void manage_ring() {

}

void display_alarm_set() {
  lcd.clear();

  lcd.setCursor(11, 1);
  lcd.print(String(alarmh) + ":" + String(alarmm));
  if (submenu == 0) {
    lcd.setCursor(11, 2);
    lcd.print("^^");
  }
  if (submenu == 1) {
    lcd.setCursor(13 ,2 );
    lcd.print("^^");
  }

}

void display_status(String s) {
  lcd.clear();
  lcd.setCursor(1, 0); lcd.print(s);

}

void update_lcd() {
  // Managing inversion of the screen (when alarm ring or not)
  if (alarm_ring) {
    if (myTZ.second() % 2 == 0) {
      lcd.noBacklight();
    } else {
      lcd.backlight();
    }
  } else {
    lcd.backlight();
  }
  // Display main screen
  if (menu_state == MENU_NONE) {
    if (millis() - last_update > 1000) {
      last_update = millis();
      dots = !dots;
      display_time();
    }
  }
  if (menu_state == MENU_ALARM_SET) {
    if (changed) {
      display_alarm_set();
      changed=false;
    } 
    return;
  }
  if (changed) {
    display_menu(menu_state);
    changed = false;
  }


}
