#define MENU_NONE 0
#define MENU_MAIN 1
#define MENU_RADIO 2
#define MENU_ALARM  3
#define MENU_LISTEN  4
#define MENU_ALARM_SET 5

#define ARRAYSIZE 4
String menu[5][ARRAYSIZE] =
{ {},
  {"Alarm", "Alarm Chanel", "Radio", "Quit"},
  {"F.I.P.", "PFM", "Planet FM", "Quit"},
  {"Set Time", "Activate", "Duration", "Quit"},
  {"Play", "Stop", "", "Quit"}
};

bool changed=false;

void manage_menu() {
  Serial.printf("Menustate=%d \n" , menu_state);

  // Crappy shit to avoid a bug : main menu displayed at startup 
  if (millis()<10000) return;
  
  switch (menu_state) {
    case MENU_NONE:
      // Clock mode so entering menu mode :
      menu_state = MENU_MAIN;
      Serial.println("Entering Main ");
      menu_pos = 0;
      break;

    // Each item of main menu :
    case MENU_MAIN :
      Serial.println("in Main");
      if (menu_pos == 0 )  menu_state = MENU_ALARM;
      if (menu_pos == 1 )  menu_state = MENU_RADIO;
      if (menu_pos == 2 )  menu_state = MENU_LISTEN;
      if (menu_pos == 3 )  menu_state = MENU_NONE;
      break;

    case MENU_RADIO :
      Serial.println("Entering RADIO");
      if (menu_pos < 3) {
        alarmc = menu_pos;
        menu_state = MENU_MAIN;
        EEPROM.write(3, menu_pos);
        EEPROM.commit();
        audio.connecttohost(stations[alarmc]);
      }
      if (menu_pos == 3 )  menu_state = MENU_MAIN;
      break;
    case MENU_ALARM :
      Serial.println("Entering ALARM");

      if (menu_pos == 0 ) {
        //set time
        menu_state = MENU_ALARM_SET;
        submenu = 0;
      }

      if (menu_pos == 1 )  {
        //activate
        Serial.println("Entering activate");
        menu_state = MENU_NONE;
        alarm_set =  !alarm_set;
        EEPROM.write(1, alarm_set);
        EEPROM.commit();
      }

      if (menu_pos == 2 )  menu_state = MENU_LISTEN;
      if (menu_pos == 3 )  menu_state = MENU_MAIN;

      break;
    case MENU_LISTEN :
      Serial.println("Entering DISPLAY");
      if (menu_pos == 0 )  {
        menu_state = MENU_MAIN;
        radio_state= 1;
      }

      if (menu_pos == 1 )  {
        menu_state = MENU_MAIN;
        radio_state= 0;
      }
      if (menu_pos == 2 )  menu_state = MENU_LISTEN;
      if (menu_pos == 3 )  menu_state = MENU_MAIN;
      break;

    case MENU_ALARM_SET:
      Serial.println("Entering ALARM SET");
      if (submenu == 0) {
        EEPROM.write(0, alarmh);
        submenu = 1;
        return;
      }
      if (submenu == 1) {
        submenu = 2;
        EEPROM.write(1, alarmm);
        EEPROM.commit();
        menu_state = MENU_ALARM;
      }
      break;
  }
}
