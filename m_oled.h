
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSansBold12pt7b.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
//SDA = 21 SCL=22
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
#define alarm_width 16
#define alarm_height 16
const unsigned char alarm_bits[] PROGMEM = {
  0x38, 0x1c, 0x7c, 0x3e, 0x70, 0x0e, 0x67, 0xe6, 0x5e, 0x7a, 0x18, 0x18, 0x30, 0x8c, 0x30, 0x8c,
  0x61, 0x86, 0x61, 0x86, 0x30, 0x64, 0x30, 0x0c, 0x38, 0x1c, 0x1c, 0x38, 0x1f, 0xf8, 0x13, 0xc8

};


void setup_oled() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.clearDisplay();

  display.ssd1306_command(SSD1306_SETCONTRAST);
  display.ssd1306_command(1); // Where c is a value from 0 to 255 (sets contrast e.g. brightness)

  display.ssd1306_command(SSD1306_DISPLAYOFF); // To switch display off


  display.ssd1306_command(SSD1306_DISPLAYON); // To switch display back on
  display.display();
}
void display_status(char *mesg) {
  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.println(mesg);
  display.display();
}


void display_time() {
  char charBuf[50];
  Time.toCharArray(charBuf, 50) ;
  display.setFont(&FreeSansBold12pt7b);
  display.clearDisplay();
  display.setCursor(0, 16);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println(Time);
  // Print alarm time :
  display.setFont();
  display.setCursor(0, 20);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println(String(alarmh) + ":" + String(alarmm) + "   [" + String(alarmc + 1) + ']');

  // Clock position :
  int cx = display.width() - 15;
  int cy = display.height() / 2;
  // Clock circle
  display.drawCircle(cx, cy, 14, WHITE);
  // Definining minute angle :
  float m = minute();
  m = m * 6; // 60min * 6 -> 360 deg
  m = m + 270; // Good orientation
  m = m * 0.0175; // Convertin to radian
  display.drawLine(cx, cy, cx + 10 * cos(m), cy + 10 * sin(m), WHITE);
  // Defining hour angle
  float h = hourFormat12() + 1;
  h = h * 30; // 12h * 30 -> 360 deg
  h = h + 270; // Good orientation
  h = h * 0.0175; // Convertin to radian
  display.drawLine(cx, cy, cx + 8 * cos(h), cy + 8 * sin(h), WHITE);

  float s = second();
  s = s * 6; // 12h * 30 -> 360 deg
  s = s + 270; // Good orientation
  s = s * 0.0175; // Convertin to radian
  display.drawLine(cx + 11 * cos(s), cy + 11 * sin(s), cx + 16 * cos(s), cy + 16 * sin(s), WHITE);

  if (alarm_set) {
    display.drawBitmap(display.width() / 2, 0, alarm_bits, alarm_width, alarm_height, 1);
  }


  display.display();

}


void display_menu(int _menu) {
  display.invertDisplay(false);
  display.clearDisplay();
  display.setFont();
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.setTextColor(WHITE);

  for (int i = 0; i <= 3; i++) {
    display.setCursor(10, i * 8);
    display.println(menu[_menu][i]);
  }
  display.setCursor(0, 8 * menu_pos);
  display.print(">");
  display.display();


}

void display_alarm_set() {
  char charBuf[50];
  Time.toCharArray(charBuf, 50) ;
  display.setFont(&FreeSansBold12pt7b);
  display.clearDisplay();
  display.setCursor(0, 16);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.print(String(alarmh) + ":" + String(alarmm));
  if (submenu == 0) {
    display.drawLine(0, 18, 25, 18, WHITE);
    display.drawLine(0, 19, 25, 19, WHITE);
  }
  if (submenu == 1) {
    display.drawLine(35, 18, 60, 18, WHITE);
    display.drawLine(35, 19, 60, 19, WHITE);
  }
  display.display();
}

void manage_ring(){
      if (myTZ.second() % 2 == 0) {
        display.invertDisplay(true);
      } else {
        display.invertDisplay(false);
      }
  
}

void update_oled() {
    if (changed) {
    display_menu(menu_state);
    changed = false;
  }
}
