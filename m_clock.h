#include <WiFiUdp.h>
#include <ezTime.h>
String Time = "";
byte last_second, second_, minute_, hour_, day_, month_;
int year_;
Timezone myTZ;

void setup_clock() {
  myTZ.setLocation(F("Europe/Paris"));
  waitForSync();
}

void update_time() {
  Time = myTZ.dateTime("H:i");
  if (!alarm_ring){
    if (alarmh==myTZ.hour() && alarmm==myTZ.minute() && myTZ.second()<=5){
      alarm_ring=true;
    }
  }
}
